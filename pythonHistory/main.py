import pygame
import random
from time import sleep

pygame.init()

screen_width = 650
screen_height = 700

screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()
fps = 60

pygame.display.set_caption('Atak na Anglie')
icon = pygame.image.load('img/Extra/1947topres.jpg')
pygame.display.set_icon(icon)

explosion_sound = pygame.mixer.Sound('img/Sounds/Explosion.wav')
explosion_sound.set_volume(0.5)
shot_sound = pygame.mixer.Sound('img/Sounds/Shot.wav')
shot_sound.set_volume(0.5)

background = pygame.image.load('img/Extra/Background.png')
prolog1 = pygame.image.load('img/Extra/prolog1.jpg')
prolog1 = pygame.transform.scale(prolog1, (500, 250))
prolog2 = pygame.image.load('img/Extra/prolog2.jpg')
prolog2 = pygame.transform.scale(prolog2, (500, 250))

prolog = True
level1 = True
game_start = True
level2 = True
level3 = True

font30 = pygame.font.SysFont('Nova Square', 30)
font40 = pygame.font.SysFont('Nova Square', 40)

game_over = 0
count = 0
countdown = 3
last_tick = pygame.time.get_ticks()

last_alien_shot = pygame.time.get_ticks()
alien_cooldown = 1000

white = (255, 255, 255)


def death():
    image = pygame.image.load('img/Extra/game_over.jpg')
    while True:
        clock.tick(fps)
        screen.fill((13, 15, 0))
        screen.blit(image, (90, 100))

        draw_text('Przegrana', font40, (255, 255, 255),
                  int(screen_width / 2 - 215), int(screen_height - 200))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

        pygame.display.update()


def draw_background():
    screen.blit(background, (0, 0))


def create_aliens(level):
    columns = 5
    final = 1
    if level == 3:
        columns = 3
        final = 2
    if level == 2:
        columns = 4
    for row in range(4):
        for item in range(columns):
            enemy = Enemy(100 + item * 100 * final, 100 + row * 70, level)
            enemy_group.add(enemy)


def draw_text(text, font, color, x, y):
    img = font.render(text, True, color)
    screen.blit(img, (x, y))


class SpaceShip(pygame.sprite.Sprite):
    def __init__(self, x, y, health):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/player/Idle.png')
        self.image = pygame.transform.scale(self.image, (28 * 3, 26 * 3))
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.health_start = health
        self.health_remaining = health
        self.last_shot = pygame.time.get_ticks()
        self.bullets = 4
        self.reloading = False
        self.reloading_start = 0

    def update(self):
        speed = 8
        cooldown = 500

        key = pygame.key.get_pressed()
        if key[pygame.K_LEFT] and self.rect.left > 0:
            self.rect.x -= speed
        if key[pygame.K_RIGHT] and self.rect.right < screen.get_width():
            self.rect.x += speed

        self.mask = pygame.mask.from_surface(self.image)

        time_in_now = pygame.time.get_ticks()

        if key[pygame.K_SPACE] and time_in_now - self.last_shot > cooldown:
            if self.bullets > 0:
                shot_sound.play()
                self.bullets -= 1
                bullet = Bullet(self.rect.centerx, self.rect.top)
                bullet_group.add(bullet)
                self.last_shot = pygame.time.get_ticks()
            else:
                if not self.reloading:
                    self.reloading = True
                    self.reloading_start = pygame.time.get_ticks()

        pygame.draw.rect(screen, 'red', (self.rect.x, (self.rect.bottom + 10), self.rect.width, 15))
        if self.health_remaining > 0:
            pygame.draw.rect(screen, 'green', (self.rect.x, (self.rect.bottom + 10),
                                               int(self.rect.width * (self.health_remaining / self.health_start)), 15))
        elif self.health_remaining <= 0:
            global game_over
            explosion = Explosion(self.rect.centerx, self.rect.centery, 3)
            explosion_group.add(explosion)
            self.kill()
            game_over = -1

        if self.reloading:
            if time_in_now - self.reloading_start > 2000:
                self.bullets = 4
                self.reloading = False
            else:
                draw_text('Reloading...', font30, (255, 255, 255),
                          screen_width - 100,
                          50)
        else:
            for ammo in range(self.bullets):
                cube = pygame.Rect(screen_width - (50 * (ammo + 1)), 50, 20, 40)
                pygame.draw.rect(screen, (255, 255, 0), cube)


class Enemy(pygame.sprite.Sprite):
    def __init__(self, x, y, level):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/enemy/Level' + str(level) + '.png')
        self.image = pygame.transform.flip(self.image, False, True)
        self.image = pygame.transform.scale(self.image, (65, 65))
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.move_count = 0
        self.move_direction = 1
        if level > 1:
            self.move_direction = 2
        self.health = 1 * level

    def update(self):
        self.rect.x += self.move_direction
        self.move_count += 1
        if self.rect.left <= 0 or self.rect.right >= screen_width:
            for enemy in enemy_group.sprites():
                enemy.flip()
        if pygame.sprite.spritecollide(self, bullet_group, True):
            self.health -= 1
            if self.health <= 0:
                self.kill()
                explosion = Explosion(self.rect.centerx, self.rect.centery, 2)
                explosion_group.add(explosion)

            # explosion_fx.play()
            explosion = Explosion(self.rect.centerx, self.rect.centery, 1)
            explosion_group.add(explosion)

    # def ai(self):
    #     our_x = self.rect.centerx
    #     self.rect.x += self.move_direction
    #     self.move_count += 1
    #     if self.rect.left == 0 or self.rect.right == screen_width:
    #         for enemy in enemy_group.sprites():
    #             enemy.flip()
    #     if not self.rect.left == 0 or not self.rect.right == screen_width:
    #         for bullet in bullet_group.sprites():
    #             bullet_x = bullet.rect.centerx
    #             if our_x <= bullet_x <= our_x + 300:
    #                 if not self.rect.left == 0 or not self.rect.right == screen_width:
    #                     self.rect.x += -1
    #                     self.move_count += 1
    #
    #             if our_x >= bullet_x >= our_x - 300:
    #                 self.rect.x += 1
    #                 self.move_count += 1

    def flip(self):
        self.move_direction *= -1
        self.move_count *= self.move_direction


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/Bullets/player_bullet.png')
        self.image = pygame.transform.scale(self.image, (8 * 2, 6 * 2))
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]

    def update(self):
        self.rect.y -= 5
        if self.rect.bottom < 0:
            self.kill()


class Alien_Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/Bullets/bullet.png')
        self.image = pygame.transform.scale(self.image, (15, 15))
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]

    def update(self):
        self.rect.y += 2
        if self.rect.top > screen_height:
            self.kill()
        if pygame.sprite.spritecollide(self, player_group, False, pygame.sprite.collide_mask):
            self.kill()
            # explosion2_fx.play()
            player.health_remaining -= 1
            explosion = Explosion(self.rect.centerx, self.rect.centery, 2)
            explosion_group.add(explosion)


class Super_Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/Bullets/super_bullet.png')
        self.image = pygame.transform.scale(self.image, (15, 20))
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]

    def update(self):
        self.rect.y += 2
        if self.rect.top > screen_height:
            self.kill()
        if pygame.sprite.spritecollide(self, player_group, False, pygame.sprite.collide_mask):
            self.kill()
            # explosion2_fx.play()
            player.health_remaining -= 1.5
            explosion = Explosion(self.rect.centerx, self.rect.centery, 2)
            explosion_group.add(explosion)


class Explosion(pygame.sprite.Sprite):
    def __init__(self, x, y, size):
        explosion_sound.play()
        self.images = []
        pygame.sprite.Sprite.__init__(self)
        for num in range(3):
            image = pygame.image.load(f'img/Explosion/{num}.png')
            if size == 1:
                image = pygame.transform.scale(image, (20, 20))
            if size == 2:
                image = pygame.transform.scale(image, (40, 40))
            if size == 3:
                image = pygame.transform.scale(image, (160, 160))
            self.images.append(image)
        self.index = 0
        self.image = self.images[self.index]
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.counter = 0

    def update(self):
        explosion_speed = 5

        self.counter += 1
        if self.counter >= explosion_speed and self.index < len(self.images) - 1:
            self.counter = 0
            self.index += 1
            self.image = self.images[self.index]
        if self.index >= len(self.images) - 1 and self.counter >= explosion_speed:
            self.kill()


enemy_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()
explosion_group = pygame.sprite.Group()
enemy_bullet_group = pygame.sprite.Group()

while game_start:
    clock.tick(fps)
    screen.fill((13, 15, 0))

    count += 1
    draw_text('Strzałki - Ruch', font40, (255, 255, 255),
              int(screen_width / 2 - 215), int(screen_height / 2 - 200))
    draw_text('Spacja - Strzał', font40, (255, 255, 255),
              int(screen_width / 2 - 215), int(screen_height / 2 - 100))
    draw_text('Atak na Anglie 10 lipca 1940 roku', font40, (255, 255, 255),
              int(screen_width / 2 - 215), int(screen_height - 200))
    draw_text('Wsieslaw Matskevicz 8D', font40, (255, 255, 255),
              int(screen_width / 2 - 170), int(screen_height / 2))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    if count >= 400:
        game_start = False

    pygame.display.update()

count = 0
while prolog:
    clock.tick(fps)
    screen.fill((13, 15, 0))

    if count <= 900:
        screen.blit(prolog1, (90, 100))
        draw_text('Po pokonaniu Francji, Adolf Hitler oczekiwał', font30, (255, 255, 255), int(screen_width / 2 - 225),
                  int(screen_height / 2 + 50))
        draw_text('że Wielka Brytania wycofa się z wojny.', font30, (255, 255, 255), int(screen_width / 2 - 225),
                  int(screen_height / 2 + 80))
        draw_text('Jednak rząd brytyjski pod przewodnictwem', font30, (255, 255, 255), int(screen_width / 2 - 225),
                  int(screen_height / 2 + 110))
        draw_text('premiera Winstona Churchilla', font30, (255, 255, 255),
                  int(screen_width / 2 - 225), int(screen_height / 2 + 140))
        draw_text('odrzucił niemieckie sugestii pokojowe.', font30, (255, 255, 255),
                  int(screen_width / 2 - 225), int(screen_height / 2 + 170))
    else:
        screen.blit(prolog2, (90, 100))
        draw_text('W bitwie o Anglię najpierw w składzie, a potem u boku', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 40))
        draw_text('walczyły 4 polskie dywizjony - 2 bombowe i 2 myśliwskie:', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 70))
        draw_text('300, 301, 302 i 303 ', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 100))
        draw_text('oraz 81 polskich pilotów w dywizjonach brytyjskich.', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 130))
        draw_text('W sumie 144 polskich pilotów.', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 160))

    count += 1

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    if count >= 1800:
        prolog = False

    pygame.display.update()

count = 0
player = SpaceShip(int(screen_width / 2), screen_height - 100, 3)
player_group = pygame.sprite.Group()
player_group.add(player)

create_aliens(1)
while level1:
    clock.tick(fps)
    draw_background()

    if countdown == 0:
        time_now = pygame.time.get_ticks()
        if time_now - last_alien_shot > alien_cooldown and len(enemy_bullet_group) < 5 and len(
                enemy_group) > 0 and game_over == 0:
            attacking_alien = random.choice(enemy_group.sprites())
            alien_bullet = Alien_Bullet(attacking_alien.rect.centerx, attacking_alien.rect.bottom)
            enemy_bullet_group.add(alien_bullet)
            last_alien_shot = time_now

        if len(enemy_group) <= 0:
            game_over = 1
        if game_over == 0:
            bullet_group.update()
            enemy_bullet_group.update()
            player.update()
            enemy_group.update()
        elif game_over == -1:
            death()
        elif game_over == 1:
            draw_text('YOU WIN!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
            count += 1
            if count >= 180:
                level1 = False
    explosion_group.update()

    explosion_group.draw(screen)
    player_group.draw(screen)
    enemy_group.draw(screen)
    bullet_group.draw(screen)
    enemy_bullet_group.draw(screen)

    if countdown > 0:
        draw_text('LEVEL 1', font40, white, int(screen_width / 2 - 55), int(screen_height / 2 + 10))
        draw_text('GET READY!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
        draw_text(str(countdown), font40, white, int(screen_width / 2 - 10), int(screen_height / 2 + 100))
        count_ticks = pygame.time.get_ticks()
        if count_ticks - last_tick >= 1000:
            countdown -= 1
            last_tick = count_ticks

    # Event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    pygame.display.update()

game_over = 0
count = 0
prolog = True
prolog1 = pygame.image.load('img/Extra/prolog12.jpg')
prolog1 = pygame.transform.scale(prolog1, (500, 250))
prolog2 = pygame.image.load('img/Extra/prolog22.jpg')
prolog2 = pygame.transform.scale(prolog2, (500, 250))

while prolog:
    clock.tick(fps)
    screen.fill((13, 15, 0))

    if count <= 900:
        screen.blit(prolog1, (90, 100))
        draw_text('Niemiecki plan przewidywał w pierwszym etapie', font30, (255, 255, 255), int(screen_width / 2 - 225),
                  int(screen_height / 2 + 50))
        draw_text('zdobycie dominacji w powietrzu dzięki', font30, (255, 255, 255), int(screen_width / 2 - 225),
                  int(screen_height / 2 + 80))
        draw_text('przełamaniu sił oraz całkowitemu rozbiciu', font30, (255, 255, 255), int(screen_width / 2 - 225),
                  int(screen_height / 2 + 110))
        draw_text('brytyjskiego lotnictwa, a także zniszczeniu ', font30, (255, 255, 255),
                  int(screen_width / 2 - 225), int(screen_height / 2 + 140))
        draw_text('lotnisk oraz infrastruktury naziemnej', font30, (255, 255, 255),
                  int(screen_width / 2 - 225), int(screen_height / 2 + 170))
    else:
        screen.blit(prolog2, (90, 100))
        draw_text('Początkowo Niemcy mieli znaczącą przewagę.Luftwaffe', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 40))
        draw_text('dysponowała 990 myśliwcami i 1640 bombowcami.', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 70))
        draw_text('Królewskie Siły Lotnicze mogły tej potędze', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 100))
        draw_text('przeciwstawić 960 samolotów myśliwskich', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 130))
        draw_text('oraz 400 bombowców.', font30, (255, 255, 255),
                  int(screen_width / 2 - 245), int(screen_height / 2 + 160))

    count += 1

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    if count >= 1800:
        prolog = False

    pygame.display.update()

count = 0
player = SpaceShip(int(screen_width / 2), screen_height - 100, 3)
player_group = pygame.sprite.Group()
player_group.add(player)
enemy_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()
explosion_group = pygame.sprite.Group()
enemy_bullet_group = pygame.sprite.Group()
create_aliens(2)
countdown = 4

while level2:
    clock.tick(fps)
    draw_background()

    if countdown == 0:
        time_now = pygame.time.get_ticks()
        if time_now - last_alien_shot > alien_cooldown and len(enemy_bullet_group) < 5 and len(
                enemy_group) > 0 and game_over == 0:
            attacking_alien = random.choice(enemy_group.sprites())
            alien_bullet = Alien_Bullet(attacking_alien.rect.centerx, attacking_alien.rect.bottom)
            enemy_bullet_group.add(alien_bullet)
            last_alien_shot = time_now

        if len(enemy_group) <= 0:
            game_over = 1
        if game_over == 0:
            bullet_group.update()
            enemy_bullet_group.update()
            player.update()
            enemy_group.update()
        elif game_over == -1:
            death()
        elif game_over == 1:
            draw_text('YOU WIN!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
            count += 1
            if count >= 180:
                level2 = False
        explosion_group.update()

        explosion_group.draw(screen)
        player_group.draw(screen)
        enemy_group.draw(screen)
        bullet_group.draw(screen)
        enemy_bullet_group.draw(screen)

    if countdown > 0:
        draw_text('LEVEL 2', font40, white, int(screen_width / 2 - 55), int(screen_height / 2 + 10))
        draw_text('GET READY!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
        draw_text(str(countdown), font40, white, int(screen_width / 2 - 10), int(screen_height / 2 + 100))
        count_ticks = pygame.time.get_ticks()
        if count_ticks - last_tick >= 1000:
            countdown -= 1
            last_tick = count_ticks

        # Event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    pygame.display.update()

game_over = 0
count = 0
prolog = True
prolog1 = pygame.image.load('img/Extra/prolog31.jpg')
prolog1 = pygame.transform.scale(prolog1, (500, 250))

while prolog:
    clock.tick(fps)
    screen.fill((13, 15, 0))

    if count <= 900:
        screen.blit(prolog1, (90, 100))
        draw_text('Hitler nie chcial się poddawać', font30, (255, 255, 255), int(screen_width / 2 - 195),
                  int(screen_height / 2 + 50))
        draw_text('wysłal elite Luftwaffe', font30, (255, 255, 255), int(screen_width / 2 - 175),
                  int(screen_height / 2 + 80))
        draw_text('na ostatnią bitwe.', font30, (255, 255, 255), int(screen_width / 2 - 155),
                  int(screen_height / 2 + 110))
        draw_text('31 Października', font30, (255, 255, 255),
                  int(screen_width / 2 - 130), int(screen_height / 2 + 140))
        draw_text('1940 Roku', font30, (255, 255, 255),
                  int(screen_width / 2 - 130), int(screen_height / 2 + 170))
    count += 1

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    if count >= 900:
        prolog = False

    pygame.display.update()

count = 0
player = SpaceShip(int(screen_width / 2), screen_height - 100, 3)
player_group = pygame.sprite.Group()
player_group.add(player)
enemy_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()
explosion_group = pygame.sprite.Group()
enemy_bullet_group = pygame.sprite.Group()
create_aliens(3)
countdown = 4
background = pygame.image.load('img/Extra/background2.jpg')

while level3:
    clock.tick(fps)
    draw_background()

    if countdown == 0:
        time_now = pygame.time.get_ticks()
        if time_now - last_alien_shot > alien_cooldown and len(enemy_bullet_group) < 5 and len(
                enemy_group) > 0 and game_over == 0:
            attacking_alien = random.choice(enemy_group.sprites())
            alien_bullet = Super_Bullet(attacking_alien.rect.centerx, attacking_alien.rect.bottom)
            enemy_bullet_group.add(alien_bullet)
            last_alien_shot = time_now

        if len(enemy_group) <= 0:
            game_over = 1
        if game_over == 0:
            bullet_group.update()
            enemy_bullet_group.update()
            player.update()
            enemy_group.update()
        elif game_over == -1:
            death()
        elif game_over == 1:
            draw_text('YOU WIN!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
            count += 1
            if count >= 180:
                level3 = False
        explosion_group.update()

        explosion_group.draw(screen)
        player_group.draw(screen)
        enemy_group.draw(screen)
        bullet_group.draw(screen)
        enemy_bullet_group.draw(screen)

    if countdown > 0:
        draw_text('FINAL LEVEL', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 10))
        draw_text('GET READY!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
        draw_text(str(countdown), font40, white, int(screen_width / 2 - 10), int(screen_height / 2 + 100))
        count_ticks = pygame.time.get_ticks()
        if count_ticks - last_tick >= 1000:
            countdown -= 1
            last_tick = count_ticks

        # Event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    pygame.display.update()

game_over = 0
count = 0
prolog = True
prolog1 = pygame.image.load('img/Extra/prolog41.png')
prolog1 = pygame.transform.scale(prolog1, (500, 250))
prolog2 = pygame.image.load('img/Extra/prolog42.png')
prolog2 = pygame.transform.scale(prolog2, (500, 250))

while prolog:
    clock.tick(fps)
    screen.fill((13, 15, 0))

    if count <= 900:
        screen.blit(prolog1, (90, 100))
        draw_text('15 września był przełomowym momentem bitwy', font30, (255, 255, 255), int(screen_width / 2 - 195),
                  int(screen_height / 2 + 50))
        draw_text('Niemcy stracili tego dnia ponad 60 samolotów', font30, (255, 255, 255), int(screen_width / 2 - 175),
                  int(screen_height / 2 + 80))
        draw_text('zginęło ponad 80 lotników niemieckich', font30, (255, 255, 255), int(screen_width / 2 - 155),
                  int(screen_height / 2 + 110))
        draw_text('wielu zostało rannych', font30, (255, 255, 255),
                  int(screen_width / 2 - 130), int(screen_height / 2 + 140))
        draw_text('lub dostało się do niewoli.', font30, (255, 255, 255),
                  int(screen_width / 2 - 130), int(screen_height / 2 + 170))
    else:
        screen.blit(prolog2, (90, 100))
        draw_text('Hitler przegrał swoją pierszą walke.', font30, (255, 255, 255), int(screen_width / 2 - 195),
                  int(screen_height / 2 + 50))
        draw_text('Ta przegrana ztała się ', font30, (255, 255, 255),
                  int(screen_width / 2 - 130), int(screen_height / 2 + 140))
        draw_text('początkiem upadku III Rzeszy', font30, (255, 255, 255),
                  int(screen_width / 2 - 130), int(screen_height / 2 + 170))
    count += 1

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    if count >= 1800:
        prolog = False

    pygame.display.update()
image = pygame.image.load('img/Extra/end of game.jpg')

while True:
    clock.tick(fps)
    screen.fill((13, 15, 0))
    screen.blit(image, (90, 100))

    draw_text('Koniec Gry', font40, (255, 255, 255),
              int(screen_width / 2 - 215), int(screen_height - 200))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    pygame.display.update()

pygame.quit()
