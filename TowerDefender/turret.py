import pygame as pg
import constants as c
import math
from turret_data import TURRET_DATA


class Turret(pg.sprite.Sprite):
    def __init__(self, sprite_sheets, tile_x, tile_y):
        pg.sprite.Sprite.__init__(self)
        self.upgrade_level = 1
        self.tile_x = tile_x
        self.tile_y = tile_y
        self.target = None
        self.range = TURRET_DATA[self.upgrade_level - 1].get('range')
        self.selected = False
        self.cooldown = TURRET_DATA[self.upgrade_level - 1].get('cooldown')
        self.last_shot = pg.time.get_ticks()
        self.sprite_sheets = sprite_sheets
        self.animation_list = self.load_sheet()
        self.frame_index = 0
        self.angle = 90
        self.original_image = self.animation_list[self.frame_index]
        self.image = pg.transform.rotate(self.original_image, self.angle)
        self.update_time = pg.time.get_ticks()
        self.x = (self.tile_x + 0.5) * c.TILE_SIZE
        self.y = (self.tile_y + 0.5) * c.TILE_SIZE
        self.rect = self.image.get_rect()
        self.rect.center = (self.x, self.y)

        self.range_image = pg.Surface((self.range * 2, self.range * 2))
        self.range_image.fill((0, 0, 0))
        self.range_image.set_colorkey((0, 0, 0))
        pg.draw.circle(self.range_image, 'gray100', (self.range, self.range), self.range)
        self.range_image.set_alpha(100)
        self.range_rect = self.range_image.get_rect()
        self.range_rect.center = self.rect.center

    def load_sheet(self):
        sprite_sheet = self.sprite_sheets[self.upgrade_level - 1]
        size = sprite_sheet.get_height()
        animation_list = []
        for x in range(c.ANIMATION_STEPS):
            temp_img = sprite_sheet.subsurface(x * size, 0, size, size)
            animation_list.append(temp_img)
        return animation_list

    def upgrade(self):
        self.upgrade_level += 1
        self.animation_list = self.load_sheet()
        self.original_image = self.animation_list[self.frame_index]
        self.range = TURRET_DATA[self.upgrade_level - 1].get('range')
        self.cooldown = TURRET_DATA[self.upgrade_level - 1].get('cooldown')
        # update circle
        self.range_image = pg.Surface((self.range * 2, self.range * 2))
        self.range_image.fill((0, 0, 0))
        self.range_image.set_colorkey((0, 0, 0))
        pg.draw.circle(self.range_image, 'gray100', (self.range, self.range), self.range)
        self.range_image.set_alpha(100)
        self.range_rect = self.range_image.get_rect()
        self.range_rect.center = self.rect.center

    def pick_target(self, enemy_group):
        for enemy in enemy_group:
            if enemy.health > 0:
                x_dist = enemy.pos[0] - self.x
                y_dist = enemy.pos[1] - self.y
                dist = math.sqrt(x_dist ** 2 + y_dist ** 2)
                if dist < self.range:
                    self.target = enemy
                    self.angle = math.degrees(math.atan2(-y_dist, x_dist))
                    self.target.health -= c.DAMAGE

    def update(self, enemy_group, world):
        if self.target:
            self.animation_update()
        else:
            if pg.time.get_ticks() - self.last_shot > (self.cooldown / world.game_speed):
                self.pick_target(enemy_group)

    def animation_update(self):
        self.original_image = self.animation_list[self.frame_index]
        self.image = self.animation_list[self.frame_index]
        if pg.time.get_ticks() - self.update_time > c.ANIMATION_DELAY:
            self.update_time = pg.time.get_ticks()
            self.frame_index += 1
            if self.frame_index >= len(self.animation_list):
                self.last_shot = pg.time.get_ticks()
                self.frame_index = 0
                self.target = None

    def draw(self, surface):
        self.image = pg.transform.rotate(self.original_image, self.angle - 90)
        self.rect = self.image.get_rect()
        self.rect.center = (self.x, self.y)
        surface.blit(self.image, self.rect)
        if self.selected:
            surface.blit(self.range_image, self.range_rect)
