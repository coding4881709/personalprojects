import math
from enemy_dta import ENEMY_DATA
import pygame as pg
from pygame.math import Vector2
import constants as c


class Enemy(pg.sprite.Sprite):
    def __init__(self, enemy_type,  waypoints, images):
        pg.sprite.Sprite.__init__(self)
        self.image = images.get(enemy_type)
        self.speed = ENEMY_DATA.get(enemy_type)['speed']
        self.health = ENEMY_DATA.get(enemy_type)['health']
        self.waypoints = waypoints
        self.pos = Vector2(waypoints[0])
        self.target = 0
        self.angle = 0
        self.movement = 0
        self.original_img = images.get(enemy_type)
        self.target_waypoint = 1
        self.image = pg.transform.rotate(self.original_img, self.angle)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos

    def update(self, world):
        self.move(world)
        self.rotate()
        self.check_alive(world)

    def move(self, world):
        if self.target_waypoint < len(self.waypoints):
            self.target = Vector2(self.waypoints[self.target_waypoint])
            self.movement = self.target - self.pos
        else:
            world.missed_enemies ++ 1
            world.health -= 1
            self.kill()
        dist = self.movement.length()
        if dist >= (self.speed * world.game_speed):
            self.pos += self.movement.normalize() * (self.speed * world.game_speed)
        else:
            if dist != 0:
                self.pos += self.movement.normalize() * dist
            self.target_waypoint += 1

    def rotate(self):
        dist = self.target - self.pos
        self.angle = math.degrees(math.atan2(-dist[1], dist[0]))
        self.image = pg.transform.rotate(self.original_img, self.angle)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos

    def check_alive(self, world):
        if self.health <= 0:
            world.killed_enemies += 1
            world.money += c.KILL_REWARD
            self.kill()
