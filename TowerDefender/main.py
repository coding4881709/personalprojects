import json
from button import Button
from turret import Turret
import constants as c
from enemy import Enemy
from world import World
import pygame as pg
import random

pg.init()

clock = pg.time.Clock()

screen = pg.display.set_mode((c.SCREEN_WIDTH + c.SIDE_PANEL, c.SCREEN_Height))
pg.display.set_caption('Tower Defense')

text_font = pg.font.SysFont('Consolas', 24, bold=True)
large_font = pg.font.SysFont('Consolas', 36)

turret_sheets = []
for x in range(c.TURRET_LEVELS):
    turret_sheet = pg.image.load(f'assets/images/turrets/turret_{x + 1}.png').convert_alpha()
    turret_sheets.append(turret_sheet)
enemies = {
    'weak': pg.image.load('assets/images/enemys/enemy_1.png').convert_alpha(),
    'medium': pg.image.load('assets/images/enemys/enemy_2.png').convert_alpha(),
    'strong': pg.image.load('assets/images/enemys/enemy_3.png').convert_alpha(),
    'elite': pg.image.load('assets/images/enemys/enemy_4.png').convert_alpha()
}

buy_turret_img = pg.image.load('assets/images/buttons/buy_turret.png').convert_alpha()
cancel_img = pg.image.load('assets/images/buttons/cancel.png').convert_alpha()
upgrade_turret_butt_img = pg.image.load('assets/images/buttons/upgrade_turret.png').convert_alpha()
begin_button_img = pg.image.load('assets/images/buttons/begin.png').convert_alpha()
restart_img = pg.image.load('assets/images/buttons/restart.png').convert_alpha()
fast_forward_img = pg.image.load('assets/images/buttons/fast_forward.png').convert_alpha()


# Variables
game_over = False
game_outcome = 0
Level_started = False
last_enemy_spawn = pg.time.get_ticks()
placing_turret = False
selected_turret = None

map_img = pg.image.load('assets/images/level/level.png').convert_alpha()

with open('assets/images/level/level.tmj') as file:
    world_data = json.load(file)


def create_turret(pos):
    pos_X = pos[0] // c.TILE_SIZE
    pos_Y = pos[1] // c.TILE_SIZE
    mouse_num = (pos_Y * c.COL) + pos_X
    if world.tile_map[mouse_num] == 7:
        space_is_free = True
        for turret in turret_group:
            if (pos_X, pos_Y) == (turret.tile_x, turret.tile_y):
                space_is_free = False
        if space_is_free:
            new_turret = Turret(turret_sheets, pos_X, pos_Y)
            turret_group.add(new_turret)
            world.money -= c.BUY_COST


def draw_text(text, font, color, x, y):
    img = font.render(text, True, color)
    screen.blit(img, (x, y))

def display_data():
    pg.draw.rect(screen, 'maroon', (c.SCREEN_WIDTH, 0, c.SIDE_PANEL, c.SCREEN_Height))

def select_turret(pos):
    pos_X = pos[0] // c.TILE_SIZE
    pos_Y = pos[1] // c.TILE_SIZE
    for turret in turret_group:
        if (pos_X, pos_Y) == (turret.tile_x, turret.tile_y):
            return turret


world = World(world_data, map_img)
world.procces_data()
world.process_enemies()

enemy_group = pg.sprite.Group()
turret_group = pg.sprite.Group()

turret_button = Button(c.SCREEN_WIDTH + 30, 120, buy_turret_img, True)
cancel_button = Button(c.SCREEN_WIDTH + 50, 180, cancel_img, False)
upgrade_turret_button = Button(c.SCREEN_WIDTH + 5, 180, upgrade_turret_butt_img, True)
begin_button = Button(c.SCREEN_WIDTH + 60, 300, begin_button_img, True)
restart_button = Button(310, 300, restart_img, True)
fast_forward_button = Button(c.SCREEN_WIDTH + 50, 300, fast_forward_img, False)


waypoints = [
    (100, 100),
    (400, 200),
    (400, 100),
    (200, 300)
]

cursor_image = pg.image.load('assets/images/turrets/cursor_turret.png').convert_alpha()

run = True

while run:
    clock.tick(c.FPS)

    screen.fill('grey100')

    world.draw(screen)
    if world.health <= 0:
        game_over = True
        game_outcome = -1
    if world.level > c.TOTAL_LEVELS:
        game_over = True
        game_outcome = 1

    if not game_over:
        display_data()
        if selected_turret:
            selected_turret.selected = True

        if Level_started:
            world.game_speed = 1
            if fast_forward_button.draw(screen):
                world.game_speed = 2
            if pg.time.get_ticks() - last_enemy_spawn > c.SPAWN_COOLDOWN:
                if world.spawned_enemies < len(world.enemy_list):
                    enemy_type = world.enemy_list[world.spawned_enemies]
                    enemy = Enemy(enemy_type, world.waypoints, enemies)
                    enemy_group.add(enemy)
                    world.spawned_enemies += 1
                    last_enemy_spawn = pg.time.get_ticks()
        else:
            if begin_button.draw(screen):
                Level_started = True

        if world.check_level():
            world.money += c.LEVEL_COMPLETE_REWARD
            world.level += 1
            Level_started = False
            last_enemy_spawn = pg.time.get_ticks()
            world.reset_level()
            world.process_enemies()

        enemy_group.update(world)
        for turret in turret_group:
            turret.update(enemy_group, world)
            turret.draw(screen)

        draw_text(str(world.health), text_font, 'grey100', 0, 0)
        draw_text(str(world.money), text_font, 'grey100', 0, 30)

        enemy_group.draw(screen)

        if turret_button.draw(screen):
            placing_turret = True
        if placing_turret:
            cursor_pos = pg.mouse.get_pos()
            cursor_rect = cursor_image.get_rect()
            cursor_rect.center = cursor_pos
            if cursor_pos[0] <= c.SCREEN_WIDTH:
                screen.blit(cursor_image, cursor_rect)

            if cancel_button.draw(screen):
                placing_turret = False
        if selected_turret:
            if selected_turret.upgrade_level < c.TURRET_LEVELS:
                if upgrade_turret_button.draw(screen) and world.money >= c.UPGRADE_COST:
                    selected_turret.upgrade()
                    world.money -= c.UPGRADE_COST
    else:
        pg.draw.rect(screen, 'dodgerblue', (200, 200, 400, 200), border_radius=30)
        if game_outcome == -1:
            draw_text('GAME_OVER', large_font, 'grey0', 310, 230)
        elif game_outcome == 1:
            draw_text('YOU WIN', large_font, 'grey0', 315, 230)
        if restart_button.draw(screen):
            game_over = False
            Level_started = False
            game_outcome = 0
            last_enemy_spawn = pg.time.get_ticks()
            placing_turret = False
            selected_turret = None
            world = World(world_data, map_img)
            world.procces_data()
            world.process_enemies()
            enemy_group.empty()
            turret_group.empty()

    for event in pg.event.get():
        if event.type == pg.QUIT:
            run = False
        if event.type == pg.MOUSEBUTTONDOWN and event.button == 1:
            mouse_pos = pg.mouse.get_pos()
            if mouse_pos[0] < c.SCREEN_WIDTH and mouse_pos[1] < c.SCREEN_Height:
                for turret in turret_group:
                    turret.selected = False
                if placing_turret and world.money >= c.BUY_COST:
                    create_turret(mouse_pos)
                else:
                    selected_turret = select_turret(mouse_pos)

    pg.display.update()

pg.quit()
