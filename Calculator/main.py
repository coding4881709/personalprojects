import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang import Builder
from kivy.core.window import Window

Window.size = (500, 700)

Builder.load_file('my.kv')


class MyLayout(Widget):
    def clear(self):
        self.ids.calc_input.text = '0'

    def button_press(self, button):
        prior = self.ids.calc_input.text

        if prior == "0":
            self.ids.calc_input.text = f'{button}'
        else:
            self.ids.calc_input.text = f'{prior}{button}'

    def math_sign(self, sign):
        prior = self.ids.calc_input.text
        self.ids.calc_input.text = f'{prior}{sign}'

    def dot(self):
        prior = self.ids.calc_input.text

        num_list = prior.split("+")

        if "." not in num_list[-1]:
            self.ids.calc_input.text = f'{prior}.'

    def remove(self):
        self.ids.calc_input.text = self.ids.calc_input.text[:-1]

    def reverse(self):
        prior = self.ids.calc_input.text
        if "-" in prior:
            self.ids.calc_input.text = prior.replace('-', '')
        else:
            self.ids.calc_input.text = f'-{prior}'

    def equals(self):
        prior = self.ids.calc_input.text
        if "Error" in prior:
            prior = ''
        try:
            answer = eval(prior)
            self.ids.calc_input.text = str(answer)
        except:
            self.ids.calc_input.text = "Error"


class CalculatorApp(App):
    def build(self):
        return MyLayout()


if __name__ == '__main__':
    CalculatorApp().run()
