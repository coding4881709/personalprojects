from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.properties import ObjectProperty
from kivy.lang import Builder
from kivy.core.window import Window

Window.size = (500, 700)

Builder.load_file('my.kv')


class MyGridLayout(GridLayout):
    pass
